#ifndef GEN_TOTALIZER_ENCODER_H
#define GEN_TOTALIZER_ENCODER_H

#include<map>
#include<vector>
#include<utility>


#include "../SimplePBConstraint.h"
#include "../IncSimplePBConstraint.h"
#include "../PBConfig.h"
#include "../clausedatabase.h"
#include "../auxvarmanager.h"
#include "../weightedlit.h"
#include "Encoder.h"

using namespace PBLib;

class gen_tot_encodert : public Encoder
{
private:
	/*
    class SWCIncData : public IncrementalData
    {
    private:
      std::vector<int32_t> outlits;
    public:
      SWCIncData(std::vector<int32_t> & outlits);
      ~SWCIncData();
      virtual void encodeNewGeq(int64_t newGeq, ClauseDatabase& formula, AuxVarManager& auxVars, std::vector< int32_t > conditionals);
      virtual void encodeNewLeq(int64_t newLeq, ClauseDatabase& formula, AuxVarManager& auxVars, std::vector< int32_t > conditionals);
    };*/

   // std::vector<int32_t> outlits;
   // bool isInc = false;

	using wlit_mapt=std::map<int64_t,int32_t>;
	using weightedlitst=std::vector<WeightedLit>;
	using wlit_pairt=std::pair<int64_t,int32_t>;
	using formulat=std::vector<std::vector<int32_t>>;
	using clauset=std::vector<int32_t>;
	using set_weightst =std::set<int64_t>;
	int32_t get_var(AuxVarManager& auxvars,wlit_mapt& oliterals,int64_t);
	bool encodeLeq(unsigned k,AuxVarManager& auxvars, formulat& formula, const weightedlitst& iliterals,wlit_mapt& oliterals);
	bool encodeGeq(unsigned k,AuxVarManager& auxvars, formulat& formula, const weightedlitst& iliterals,wlit_mapt& oliterals);
	void encode_intern(const SimplePBConstraint& pbconstraint, ClauseDatabase & formula, AuxVarManager & auxvars, bool encodeComplete = false);

	bool isSubSetSum(std::vector<int64_t> &set, int64_t sum);
	void getCombinations(std::vector<int64_t> &subset, std::vector<int64_t> &comb);


public:
    void encode(const SimplePBConstraint& pbconstraint, ClauseDatabase & formula, AuxVarManager & auxvars);
    int64_t encodingValue(const SimplePBConstraint& pbconstraint);

  //  void encode(const std::shared_ptr< IncSimplePBConstraint >& pbconstraint, ClauseDatabase& formula, AuxVarManager& auxvars);
  //  int64_t encodingValue(const std::shared_ptr< IncSimplePBConstraint >& pbconstraint);

    gen_tot_encodert(PBConfig config);
    virtual ~gen_tot_encodert();

};
struct Sum {
    Sum() { sum = 0; }
    void operator()(int n) { sum += n; }

    int sum;
};
#endif
