#include <utility>
#include <algorithm>
#include <tuple>
#include "gen_totalizer.h"

using namespace PBLib;

int32_t gen_tot_encodert::get_var(AuxVarManager& auxvars,wlit_mapt& oliterals,int64_t weight)
{
	auto it=oliterals.find(weight);
	if(it==oliterals.end())
	{
		int32_t v=auxvars.getVariable();
		oliterals[weight]=v;
	}
	return oliterals[weight];
}

gen_tot_encodert::gen_tot_encodert(PBConfig config):Encoder(config){}

gen_tot_encodert::~gen_tot_encodert(){}

bool gen_tot_encodert::isSubSetSum(std::vector<int64_t> &set, int64_t sum) {
  int64_t m = set.size();
  std::vector< std::vector<bool> > ssTable;
  ssTable.resize(sum+1);
  for (int64_t i = 0; i < sum+1; i++)
    ssTable[i].resize(m+1);

  // base cases: if m == 0 then no solution for any sum
  for (int64_t i = 0; i <= sum; i++) {
    ssTable[i][0] = false;
  }
  // base case: if sum = 0 then there is only one solution for any input set: just take none of each of the items.
  for (int64_t j = 0; j <= m; j++) {
    ssTable[0][j] = true;
  }

  for (int64_t i = 1; i <= sum; i++) {
    for (int64_t j = 1; j <= m; j++) {
      // solutions excluding last element i.e. set[j-1]
      bool s1 = ssTable[i][j - 1];
      // solutions including last element i.e. set[j-1]
      bool s2 = (i - set[j - 1]) >= 0 ? ssTable[i - set[j - 1]][j - 1] : false;

      ssTable[i][j] = s1 || s2;
    }
  }

  return ssTable[sum][m];
}

void gen_tot_encodert::getCombinations(std::vector<int64_t> &subset, std::vector<int64_t> &comb){

  int64_t i = 1;
  int64_t max = 0;
  for (int i = 0; i < subset.size(); i++)
      max += subset[i];

  while (i <= max) {
    if(isSubSetSum(subset, i)){
      comb.push_back(i);
    }
    i++;
  }
}

void gen_tot_encodert::encode(const SimplePBConstraint& pbconstraint, ClauseDatabase & formula, AuxVarManager & auxvars)
{
	unsigned k = pbconstraint.getLeq();
	formulat f;

	weightedlitst iliterals=pbconstraint.getWeightedLiterals();
	pbconstraint.print();
	int64_t largest_weight=0;
	set_weightst weights;
	std::vector<int64_t> multi_set;
	std::vector<int64_t> comb;

        for (auto &l : iliterals)
	  {
	    if (l.weight > largest_weight)
	      largest_weight = l.weight;
	    weights.insert(l.weight);
	    multi_set.push_back(l.weight);
	  }

	//getCombinations(multi_set, comb);
	/*
	int64_t max = 0;
	for (int i = 0; i < multi_set.size(); i++)
	  max += multi_set[i];
	std::cout << "lits: " << iliterals.size() << "\nrhs: " << k << "\ndiff: " << weights.size() << "\nsum: " << max << "\nmax: " << largest_weight << std::endl;
	*/
	//return;
	
	wlit_mapt oliterals;

	bool result=encodeLeq(k+1,auxvars,f,iliterals,oliterals);
	assert(result);

	formula.addClauses(f);

	formula.addConditionals(pbconstraint.getConditionals());

	formula.addClause(-oliterals.rbegin()->second);
	unsigned pbsize=pbconstraint.getConditionals().size();
	for(unsigned i=0;i<pbsize;i++)
	{
		formula.getConditionals().pop_back();
	}



}
int64_t gen_tot_encodert::encodingValue(const SimplePBConstraint& pbconstraint){}

void gen_tot_encodert::encode_intern(const SimplePBConstraint& pbconstraint, ClauseDatabase & formula, AuxVarManager & auxvars, bool encodeComplete){}
bool gen_tot_encodert::encodeLeq(unsigned k, AuxVarManager& auxvars,formulat& formula, const weightedlitst& iliterals,wlit_mapt& oliterals)
{

  if(iliterals.size()==0 || k==0) return false;

	if(iliterals.size()==1)
  {
	  oliterals.insert(wlit_pairt(iliterals.front().weight,iliterals.front().lit));
	  return true;
  }

	unsigned int size=iliterals.size();

	formulat lformula,rformula;
	weightedlitst linputs, rinputs;
	wlit_mapt loutputs,routputs;

	unsigned int lsize=size>>1;
	unsigned int rsize=size-lsize;
	weightedlitst::const_iterator myit = iliterals.begin();
	weightedlitst::const_iterator myit1=myit+lsize;
	weightedlitst::const_iterator myit2=iliterals.end();

	linputs.insert(linputs.begin(),myit,myit1);
	rinputs.insert(rinputs.begin(),myit1,myit2);


	int64_t lk=0;
	for (auto &l : linputs)
	{
		lk+=l.weight;
	}
	int64_t rk=0;
		for (auto &l : rinputs)
		{
		  rk+=l.weight; // RM: fixed lk to rk
		}
	//since the totalizer sum is always upto k+1,
	//if lsize many input literals are passed, value of
	// k should be lsize-1
	lk=k>=lk?lk:k;
	rk=k>=rk?rk:k;

	bool result=encodeLeq(lk,auxvars,lformula,linputs,loutputs);
	if(!result) return result;
	result = result && encodeLeq(rk,auxvars,rformula,rinputs,routputs);
	if(!result) return result;

if(!lformula.empty())
{
	std::move(lformula.begin(),lformula.end(),std::back_inserter(formula));
}
{
	assert(!loutputs.empty());

	for(auto &l : loutputs)
		{
			clauset clause;
			clause.push_back(-l.second);
			if(l.first>k)
					{
					clause.push_back(get_var(auxvars,oliterals,k));
					}
					else
					{
						clause.push_back(get_var(auxvars,oliterals,l.first));
					}

					formula.push_back(std::move(clause));


		}
}
if(!rformula.empty())
{
	std::move(rformula.begin(),rformula.end(),std::back_inserter(formula));
}
{
	assert(!routputs.empty());
	for(auto &r : routputs)
			{
				clauset clause;
				clause.push_back(-r.second);
				if(r.first>k)
						{
						clause.push_back(get_var(auxvars,oliterals,k));
						}
						else
						{
							clause.push_back(get_var(auxvars,oliterals,r.first));
						}

						formula.push_back(std::move(clause));


			}
}

//if(!lformula.empty() && !rformula.empty())
{
	for (auto &l : loutputs)
	{
		for(auto &r: routputs)
		{
			clauset clause;
			clause.push_back(-l.second);
			clause.push_back(-r.second);
			int64_t tw=l.first+r.first;
			if(tw>k)
			{
			clause.push_back(get_var(auxvars,oliterals,k));
			}
			else
			{
				clause.push_back(get_var(auxvars,oliterals,tw));
			}

			formula.push_back(std::move(clause));
		}
	}

}

#if 0
// boost unit propagation
{
//if higher weight is 1, so is the lower weight
	//similarly the following also enforces that
	//if the lower weight is 0 so is the higher weight
auto prev=oliterals.rbegin();
auto it=prev;
it++;
for(;it!=oliterals.rend();it++)
{
	clauset clause;
	clause.push_back(-prev->second);
	clause.push_back(it->second);
	formula.push_back(std::move(clause));
}
}
#endif

	return true;

}
bool gen_tot_encodert::encodeGeq(unsigned k, AuxVarManager& auxvars,formulat& formula, const weightedlitst& iliterals,wlit_mapt& oliterals)
{

  if(iliterals.size()==0 || k==0) return false;

	if(iliterals.size()==1)
  {
	  oliterals.insert(wlit_pairt(iliterals.front().weight,iliterals.front().lit));
	  return true;
  }

	unsigned int size=iliterals.size();

	formulat lformula,rformula;
	weightedlitst linputs, rinputs;
	wlit_mapt loutputs,routputs;

	unsigned int lsize=size>>1;
	unsigned int rsize=size-lsize;
	weightedlitst::const_iterator myit = iliterals.begin();
	weightedlitst::const_iterator myit1=myit+lsize;
	weightedlitst::const_iterator myit2=iliterals.end();

	linputs.insert(linputs.begin(),myit,myit1);
	rinputs.insert(rinputs.begin(),myit1,myit2);


	int64_t lk=0;
	for (auto &l : linputs)
	{
		lk+=l.weight;
	}
	int64_t rk=0;
		for (auto &l : rinputs)
		{
			lk+=l.weight;
		}
	//since the totalizer sum is always upto k+1,
	//if lsize many input literals are passed, value of
	// k should be lsize-1
	lk=k>=lk?lk:k;
	rk=k>=rk?rk:k;

	bool result=encodeGeq(lk,auxvars,lformula,linputs,loutputs);
	if(!result) return result;
	result = result && encodeGeq(rk,auxvars,rformula,rinputs,routputs);
	if(!result) return result;

	/**********************/
/** populate all possible weight combination **/
	std::set<int64_t> wset;
	for(auto& v:loutputs)
	{
		wset.insert(v.first);
	}
	for(auto& v:routputs)
		{
			wset.insert(v.first);
		}
	for(auto& v:loutputs)
		{
		for(auto& v1:routputs)
			wset.insert(v.first+v1.first);
		}
	//for 0+0=0 case to make it uniform.
	wset.insert(0);

	{
		//remove all the weights above k
		auto sit = wset.upper_bound(k);
		assert(sit!=wset.end());
		sit++;
		wset.erase(sit,wset.end());
	}

	#if 0
	/**0 0 case**/
	/* This case is taken care of by i j case **/
	{
	  clauset clause;
	  clause.push_back(loutputs.begin()->second);
	  clause.push_back(routputs.begin()->second);
	  clause.push_back(-(*wset.begin()));
	  formula.push_back(std::move(clause));
	}
#endif


	assert(!loutputs.empty());
	assert(!routputs.empty());

	/** copy formulas coming from left-right subtree**/
	if(!lformula.empty())
	std::move(lformula.begin(),lformula.end(),std::back_inserter(formula));
	if(!rformula.empty())
	std::move(rformula.begin(),rformula.end(),std::back_inserter(formula));
	/**  i max case**/
	{
		int64_t w1,w2;

		w2=routputs.rbegin()->first;
auto plit=loutputs.end();
		for(auto lit=loutputs.begin();lit!=loutputs.end();lit++)
		{
			if(plit==loutputs.end())
			{
				w1=0;
			}
			else
			{
				w1=plit->first;
			}
			plit=lit;
			auto it=wset.find(w1+w2);
			assert(it!=wset.end());
			it++;
			clauset clause;
			if(it!=wset.end())
			{
				clause.push_back(lit->second);
				clause.push_back(-get_var(auxvars,oliterals,*it));
				formula.push_back(std::move(clause));
			}
		}
	}
	/** max i case **/
	{
			int64_t w1,w2;

			w1=loutputs.rbegin()->first;
	auto prit=routputs.end();
			for(auto rit=routputs.begin();rit!=routputs.end();rit++)
			{
				if(prit==routputs.end())
				{
					w2=0;
				}
				else
				{
					w2=prit->first;
				}
				prit=rit;
				auto it=wset.find(w1+w2);
				assert(it!=wset.end());
				it++;
				clauset clause;
				if(it!=wset.end())
				{
					clause.push_back(rit->second);
					clause.push_back(-get_var(auxvars,oliterals,*it));
					formula.push_back(std::move(clause));
				}
			}
		}

		/** i j case **/
		{
			auto plit=loutputs.end();
			auto prit=routputs.end();
			for(auto lit=loutputs.begin();lit!=loutputs.end();lit++)
			{
				for(auto rit=routputs.begin();rit!=routputs.end();rit++)
				{


					int64_t w1,w2;
					int32_t l1,l2;
					if(plit==loutputs.end())
					{
					   w1=0;

					}
					else
					{
						w1=plit->first;
					}
					plit=lit;
					if(prit==routputs.end())
					{
						w2=0;
					}
					else
					{
						w2=prit->first;
					}
					prit=rit;
					auto it=wset.find(w1+w2);
					assert(it!=wset.end());
					it++;
					clauset clause;
					if(it!=wset.end())
					{
					  clause.push_back(lit->second);
					  clause.push_back(rit->second);
					  clause.push_back(-get_var(auxvars,oliterals,*it));
					  formula.push_back(std::move(clause));

					}
				}
			}
		}



	return true;

}
